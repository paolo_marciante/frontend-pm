import { Detail } from './components/Detail';
import { Home } from './components/Home';
import { Navbar } from './components/Navbar';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

export const App = () => {
  return (
    <>
      <Navbar />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="dettaglio/:id" element={<Detail />} />
        </Routes>
      </Router>
    </>
  );
};
