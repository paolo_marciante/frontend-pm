import React, { useEffect, useState } from 'react';
import { Card } from './Card/Card';
import { Link } from 'react-router-dom';
import apiCar from '../api/apiCar';

export const Home = () => {
  const [cars, setCars] = useState([]);
  const [errBe, setErrBe] = useState('');

  useEffect(() => {
    const fetchCars = async () => {
      try {
        const response = await apiCar.get('/getAllCarInside');
        setCars(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErrBe(`Errore: ${e.message} `);
          console.log(errBe);
        }
      }
    };
    fetchCars();
  }, [errBe]);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4 g-4 mt-5">
              {cars.map((car) => (
                <Card
                  marchioAuto={car.marchio}
                  modelloAuto={car.modello}
                  annoDiProduzione={car.annoProduzione}
                  colore={car.colore}
                  cilindrata={car.cilindrata}
                  dettaglio={
                    <Link
                      key={car.idConfigurazioneModello}
                      to={`dettaglio/${car.idConfigurazioneModello}`}
                    >
                      <button
                        type="button"
                        className="btn btn-success"
                      >
                        Vai al dettaglio
                      </button>
                    </Link>
                  }
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
