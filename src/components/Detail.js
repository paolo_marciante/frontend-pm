import React, { useState, useEffect } from 'react';
import apiCar from '../api/apiCar';
import { useParams } from 'react-router-dom';

export const Detail = () => {
  const params = useParams();
  const [err, setErr] = useState('');
  const [car, setCar] = useState({});

  useEffect(() => {
    const fetchPost = async () => {
      try {
        const response = await apiCar.get(
          `/getAllCarInside/${params.id}`
        );
        setCar(response.data);
      } catch (e) {
        if (e.response) {
          console.log(e.response.data);
          console.log(e.response.status);
          console.log(e.response.headers);
        } else {
          setErr(`Errore: ${e.message}`);
          console.log(err);
        }
      }
    };
    fetchPost();
  }, [err, params]);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1 className="text-center mt-4">Nome Auto</h1>
          <img
            className="rounded mx-auto d-block m-5 img-fluid"
            src="https://www.fiat.it/content/dam/fiat/cross/models/500-family-2020/opening/desktop/figurini/500/Hey-Google/500-hey-google-Model-page-gelato-white-glossy-black-Car-Desktop-680x430.png"
            alt=""
          />
          <div className="row">
            <div className="col-md-6 mx-auto"></div>
            <h3 className="text-center">marchio modello </h3>
            <div className="row">
              <div className="col-md-6 mx-auto mt-5">
                <p>
                  Alimentazione: <b>{car.alimentazione}</b>
                </p>
                <p>
                  Cilindrata: <b>{car.cilindrata}</b>
                </p>
                <p>
                  Tipo Cambio: <b>{car.tipoCambio}</b>
                </p>
                <p>
                  Colore: <b>{car.colore}</b>
                </p>
                <p>
                  Anno di produzione: <b>{car.annoDiProduzione}</b>
                </p>
                <p>
                  Kw_T: <b>{car.kw_T}</b>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
