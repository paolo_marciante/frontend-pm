import React from 'react';
import { Link } from 'react-router-dom';
import { auto } from '../../api/response';

export const Card = ({
  marchioAuto,
  modelloAuto,
  annoDiProduzione,
  colore,
  cilindrata,
  dettaglio,
}) => {
  return (
    <div class="col">
      <div class="card" style={{ width: '18rem' }}>
        <img
          src="https://www.fiat.it/content/dam/fiat/cross/models/500-family-2020/opening/desktop/figurini/500/Hey-Google/500-hey-google-Model-page-gelato-white-glossy-black-Car-Desktop-680x430.png"
          class="card-img-top"
          alt="..."
        />
        <div class="card-body">
          <h5 class="card-title mb-4">
            {marchioAuto} - {modelloAuto}
          </h5>
          <p class="card-text">
            Anno di produzione: <b>{annoDiProduzione}</b>{' '}
          </p>
          <p class="card-text">
            {' '}
            Colore: <b>{colore}</b>{' '}
          </p>
          <p class="card-text">
            {' '}
            Cilindrata: <b>{cilindrata} CC</b>{' '}
          </p>
          {dettaglio}
        </div>
      </div>
    </div>
  );
};
